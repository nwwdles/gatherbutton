--[[---------------------------------------------------------------------------
MIT License

Copyright (c) 2019 cupnoodles

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]] --------------------------------------------------------------------------
GatherButtonGcd = {}

local FrameManager_OnLButtonDown

function GatherButtonGcd.Initialize()
    GatherButtonGcd.Settings = GatherButtonGcd.DefaultSettings
    FrameManager_OnLButtonDown = FrameManager.OnLButtonDown
    FrameManager.OnLButtonDown = GatherButtonGcd.HookOnLButtonDown(
                                     FrameManager_OnLButtonDown)
end

function GatherButtonGcd.HookOnLButtonDown(fn)
    return function(flags, mouseX, mouseY)
        fn(flags, mouseX, mouseY)
        local frame = FrameManager:GetActiveWindow()
        GatherButtonGcd.UpdateButton(frame)
    end
end

function GatherButtonGcd.Unhook()
    FrameManager.OnLButtonDown = FrameManager_OnLButtonDown
end

function GatherButtonGcd.UpdateButton(frame)
    if (frame.m_ActionType ~= GameData.PlayerActions.DO_ABILITY) or
        (not frame.m_ActionId) then
        if frame.m_ActionId == GatherButtonMacro.MacroId then
            GatherButton.SelectNextReady()
        end

        return
    end

    if (frame.m_Cooldown < GatherButtonGcd.Settings.REVERT_TIME) or
        not (GatherButton.PlotReady[GameData.Player.Cultivation.CurrentPlot] or
            GatherButton.SelectNextReady()) then
        if frame.m_toggled then
            GatherButtonGcd.RevertData(frame)
            frame.m_toggled = false
        end

        return
    end

    if frame.m_toggled then
        if GatherButton.SelectNextReady() then
            return
        end

        if GatherButtonGcd.Settings.AGRESSIVE_REVERT then
            GatherButtonGcd.RevertData(frame)
        end

        return
    end

    frame.m_toggled = true
    GatherButtonGcd.ChangeData(frame)
end

function GatherButtonGcd.ChangeData(button)
    if (GatherButtonGcd.Settings.IGNORED_ABILITIES[button.m_ActionId]) then
        return
    end

    WindowSetGameActionData(button:GetName() .. "Action",
                            GameData.PlayerActions.PERFORM_CRAFTING,
                            GameData.TradeSkills.CULTIVATION, L"")
end

function GatherButtonGcd.RevertData(button)
    WindowSetGameActionData(button:GetName() .. "Action", button.m_ActionType,
                            button.m_ActionId, L"")
end
