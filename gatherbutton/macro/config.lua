GatherButtonMacro.DefaultSettings = {
    -- To print icons to debug console (open with /d) you can use this command:
    --      /script for i = 1, 299 do d(towstring(i)..L" <icon"..towstring(i)..L">")end
    --
    -- IMPORTANT: Macro.Text values must be unique across all macros, they're
    -- used determine macros' existense.
    GatherMacro = {Text = L"/script d(gathering)", Icon = 699, Name = L"Gather"},
    ToggleMacro = {
        Text = L"/script GatherButton.Toggle()",
        Icon = 149,
        Name = L"Toggle GatherButton"
    },

    -- Button visuals
    -- Indicate autorepeat state by coloring the gatherbutton
    TINT_ENABLED = {r = 255, g = 255, b = 255},
    TINT_DISABLED = {r = 150, g = 0, b = 0},
    -- Glow type: nil - auto, true - order, false - destro
    GLOW_TYPE_ORDER = nil,
    GLOW_TINT = {r = 255, g = 255, b = 255}
}
