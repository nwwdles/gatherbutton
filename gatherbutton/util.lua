function GatherButton.ChatPrint(line)
    TextLogAddEntry("Chat", 0,
                    towstring(GatherButton.MakeIconString(
                                  GatherButtonMacro.Settings.GatherMacro.Icon)) ..
                        towstring(" " .. line))
end

function GatherButton.MakeIconString(iconId)
    return "<icon" .. tostring(iconId) .. ">"
end
