# GatherButton

Adds a hotbar button (actually, a special macro) to gather plants and replant them.

The package consists of two addons:

- **GatherButton** - Provides the macro button and automated seed refining & planting.
- **GatherButtonGcd** - Makes hotbar buttons on cooldown act as "Gather" buttons. Kind of similar to old Twister addon but for gathering plants.

## How to use

1. **Setup macro**: open Macro window and drag GatherButton (the one with roses) to a hotbar.
2. **Plant seeds the first time**: open Cultivation Window and right-click the seeds in your backpack until they are planted.
   - Usually, there's no need to open each individual plot because GatherButton automatically selects the empty plot in the background.
   - (If you are using GatherButton with Miracle Grow, you can just plant seeds as usual).
3. **Gather**: use macro key you have set in step 1 to gather plants. Seeds will be planted again automatically.
   - You can also rely on GatherButtonGcd (below) to gather plants.

- **To change seeds**, disable autoplanting using 'Toggle GatherButton' macro or `/gabu toggle` chat command. Then swap seeds.

**Note:** Only seeds and plants from crafting bags will be planted/refined. This keeps code a bit simpler and is just my preference to have seeds in main bags untouched.

## Configuration

There is no configuration UI, since I'm personally content with the default config. And I hope that the addon will be zero-configuration for most users.

Some options may be available using `/gabu` slash command:

```txt
usage: /gabu [SUBCOMMAND]

SUBCOMMANDS:
   toggle                  disable autorepeating
   glow-type [TYPE]        set glow-type. types starting with 'o' are
                              interpreted as 'order', 'd' - 'destro',
                              everything else is 'auto'
   reset                   disable persistent settings. settings will be
                              restored to .lua values on the next UI reload
```

To change other settings, edit `config.lua`. Alternatively, you can use `/script` chat command like this (this way you're not limited to the options the chat command provides):

```txt
/script GatherButton.Settings.PERSISTENT_SETTINGS = true
/script GatherButton.Settings.SEEDS_TO_KEEP = 6; GatherButtonMacro.Settings.GLOW_TYPE_ORDER = false
```

Setting `GatherButton.Settings.PERSISTENT_SETTINGS` to `true` makes addon settings persist across reloads instead of being reset to in-file values every time. Using `/gabu` command sets it to `true` automatically (when it makes sense).

Settings migration on updates can be a lot of work and is not supported. Use `/gabu reset` to reset to in-file values if something breaks.

## Screenshots

Button with 1 finished plant and 3 plants still growing:

![](https://i.imgur.com/cdzy13X.png)

Button with 4 plants growing:

![](https://i.imgur.com/zsGlQac.png)

## Known issues

- The game might spam the chat with 'Plot still has plants in it...' and 'Plot hasn't finished growing' messages while gathering plants. Although it should be more rare with the latest updates.
  - You may want to move 'User Errors' and 'Crafting' channels to a secondary chat window.
- For simplicity, and because hardly anyone is using soil/water/nutrients, they won't be automatically added on replant (unless Miracle Grow is enabled: GatherButton will call MGR functions that have the functionality).
- Minor visual issues:
  - Cooldown text and/or stack text may become invisible due to being too long/big and not fitting in the button.
  - Dragging GatherButton and reloading UI removes the glow.
